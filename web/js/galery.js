/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var galery = {
    showMainImg: function(elem){
        $( "#mainShow" ).fadeIn(600, this.onFadeInComplete());
        $("#galeryShow").hide();
        var imgSrc = $(elem).attr("src");
        var imgTitle = $(elem).attr("data-title");
        $("#mainImage").attr("src", imgSrc);
        $("#title").html("<b>"+imgTitle+"</b>");
    },
    showGalery: function(){
       $("#mainShow").hide();
       $("#galeryShow").show(); 
    },
    onFadeInComplete: function(){
        //Empty
    },
    resizeParent: function(elem){
        if($(elem).parent().hasClass("col-lg-6")){
            $(elem).parent().removeClass("col-lg-6");
            $(elem).parent().addClass("col-lg-12");
        }else if($(elem).parent().hasClass("col-lg-12")){
            $(elem).parent().removeClass("col-lg-12");
            $(elem).parent().addClass("col-lg-6");
        }
    },
    hideTextResizeImg: function(textId,elem){
        if($(elem).parent().hasClass("col-lg-8")){
            $(elem).parent().removeClass("col-lg-8");
            $(elem).parent().addClass("col-lg-12");
            $("#"+textId).hide();
        }else if($(elem).parent().hasClass("col-lg-12")){
            $(elem).parent().removeClass("col-lg-12");
            $(elem).parent().addClass("col-lg-8");
            $("#"+textId).show();
        }
    }
};

